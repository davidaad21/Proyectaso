﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;

    public Transform visualLeftWheel;
    public Transform visualRightWheel;


    public bool motor;
    public bool steering;
}

public class CarController : MonoBehaviour
{
    public List<AxleInfo> axleInfos;
    public float forcePrimeraMarcha;
    public float forceSegundaMarcha;
    public float forceUltimaMarcha;
    public float maxVelocityFirst = 30;
    public float maxVelocitySecond = 60;
    public float maxVelocityFinal = 80;
    public float maxSteeringAngle = 45;
    private Rigidbody _rb;
    public GameObject posicionTM;
    public TextMeshProUGUI textoFinal;
    public TextMeshProUGUI textoVelocidad;
    public TextMeshProUGUI textoVida;
    private float returnMenu = 0;

    //disparo
    public AudioClip sonidoDisparo;
    AudioSource audioS;
    public GameObject objBala;
    public float timeBeforeNextShot = 0.2f;
    private float canShot = 0f;
    public float salud = 10;

    //checkpoints
    public GameObject prefabCheckpoint;
    public GameObject checkpointContainer;
    private Transform[] checkpoints;
    private int currentCheckpoint = 0;
    private GameObject checkpointDisplay;

    public TakeGuns takeGun;
    public TakeGun2 takeGun2;
    void Start()
    {
        GetCheckpoints();
        checkpointDisplay = Instantiate(prefabCheckpoint, checkpoints[currentCheckpoint].position, Quaternion.identity);
        audioS = GetComponent<AudioSource>();
        _rb = GetComponent<Rigidbody>();
    }
    
    public void ApplyLocalPositionToVisuals(WheelCollider collider, Transform visualWheel)
    {
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }
    void GetCheckpoints()
    {
        Transform[] potencialcheckpoints = checkpointContainer.GetComponentsInChildren<Transform>();
        checkpoints = new Transform[potencialcheckpoints.Length - 1];
        int i = 0;
        foreach (Transform potencialWaypoint in potencialcheckpoints)
            if (potencialWaypoint != checkpointContainer.transform)
                checkpoints[i++] = potencialWaypoint;
    }
    public void CheckpointSystem()
    {
        Vector3 RelativeWaypointPosition = transform.InverseTransformPoint(new Vector3(
            checkpoints[currentCheckpoint].position.x,
            transform.position.y,
            checkpoints[currentCheckpoint].position.z));
        if (RelativeWaypointPosition.magnitude < 5)
        {
            currentCheckpoint++;
            if (currentCheckpoint >= checkpoints.Length)
            {
                Destroy(checkpointDisplay);
                currentCheckpoint = 0;
                checkpointDisplay = Instantiate(prefabCheckpoint, checkpoints[currentCheckpoint].position, Quaternion.identity);
            }
            else
            {
                Destroy(checkpointDisplay);
                checkpointDisplay = Instantiate(prefabCheckpoint, checkpoints[currentCheckpoint].position, Quaternion.identity);
            }
        }
    }
    public void FixedUpdate()
    {
        float oc = Mathf.RoundToInt(_rb.velocity.magnitude * 3600 / 1000)*0.01f;
        audioS.pitch = oc + .5f;
        if (returnMenu != 0)
        {
            if (Time.time > returnMenu)
            {
                regresarMenu();
            }
        }
        textoVida.SetText("Vida: " + salud);
        CheckpointSystem();
        Vector2 p1 = new Vector2(transform.position.x,transform.position.z);
        Vector2 p2 = new Vector2(checkpointDisplay.transform.position.x, checkpointDisplay.transform.position.z);
        

        float velocidad = Mathf.RoundToInt(_rb.velocity.magnitude * 3600 / 1000);
        textoVelocidad.SetText("Velocidad:" + velocidad + "Km/h");
        float motor;
        if (velocidad < maxVelocityFirst)
        {
            motor = forcePrimeraMarcha * Input.GetAxis("Vertical");
        }
        else
        {
            if (velocidad > maxVelocitySecond)
            {
                motor = forceUltimaMarcha * Input.GetAxis("Vertical");
            }
            else
            {
                motor = forceSegundaMarcha * Input.GetAxis("Vertical");
            }
            
        }
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        if (Input.GetButton("Fire1") && Time.time > canShot && takeGun.vacio() && takeGun2.vacio())
        {
            audioS.PlayOneShot(sonidoDisparo);
            canShot = Time.time + timeBeforeNextShot;
            GameObject balaP0 = Instantiate(objBala, transform.GetChild(0).position, Quaternion.identity);
            GameObject balaP1 = Instantiate(objBala, transform.GetChild(1).position, Quaternion.identity);
            Rigidbody balaPrefabInstanc0 = balaP0.GetComponent<Rigidbody>();
            Rigidbody balaPrefabInstanc1 = balaP1.GetComponent<Rigidbody>();
            balaPrefabInstanc0.AddForce(transform.forward * 100 * 30);
            balaPrefabInstanc1.AddForce(transform.forward * 100 * 30);
            Destroy(balaP0, 3.0f);
            Destroy(balaP1, 3.0f);
        }
        else if (Input.GetButton("Fire1") && Time.time > canShot && takeGun2.vacio())
        {
            audioS.PlayOneShot(sonidoDisparo);
            canShot = Time.time + timeBeforeNextShot;
            GameObject balaP1 = Instantiate(objBala, transform.GetChild(1).position, Quaternion.identity);
            Rigidbody balaPrefabInstanc1 = balaP1.GetComponent<Rigidbody>();
            balaPrefabInstanc1.AddForce(transform.forward * 100 * 30);
            Destroy(balaP1, 3.0f);
        }
        else if (Input.GetButton("Fire1") && Time.time > canShot && takeGun.vacio())
        {
            audioS.PlayOneShot(sonidoDisparo);
            canShot = Time.time + timeBeforeNextShot;
            GameObject balaP0 = Instantiate(objBala, transform.GetChild(0).position, Quaternion.identity);
            Rigidbody balaPrefabInstanc0 = balaP0.GetComponent<Rigidbody>();
            balaPrefabInstanc0.AddForce(transform.forward * 100 * 30);
            Destroy(balaP0, 3.0f);
        }

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                if (Input.GetButton("Jump"))
                {
                    axleInfo.rightWheel.brakeTorque = 10000000f;
                    axleInfo.leftWheel.brakeTorque = 10000000f;
                }
                else
                {
                    if (velocidad > maxVelocityFinal)
                    {
                        axleInfo.rightWheel.brakeTorque = 0;
                        axleInfo.leftWheel.brakeTorque = 0;
                        axleInfo.leftWheel.motorTorque = 0;
                        axleInfo.rightWheel.motorTorque = 0;
                    }
                    else
                    {
                        axleInfo.leftWheel.motorTorque = motor;
                        axleInfo.rightWheel.motorTorque = motor;
                        axleInfo.rightWheel.brakeTorque = 0;
                        axleInfo.leftWheel.brakeTorque = 0;
                    }
                }
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel, axleInfo.visualLeftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel, axleInfo.visualRightWheel);
        }
    }

    public void regresarMenu()
    {
        Application.LoadLevel("Menu");
    }
    private void AnimateWheels(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Quaternion _rot;
        Vector3 _pos;

        Vector3 rotate = Vector3.zero;
        wheelCollider.GetWorldPose(out _pos, out _rot);
        wheelTransform.transform.rotation = _rot;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.IndexOf("Bala") != -1)
        {
            salud--;
            if (salud <= 0)
            {
                textoFinal.SetText("Perdiste");
                forcePrimeraMarcha = 0;
                forceSegundaMarcha = 0;
                forceUltimaMarcha = 0;
                returnMenu = 2f + Time.time;
                //Application.LoadLevel("Menu");
            }
        }
        if (collision.gameObject.name.IndexOf("Meta") != -1)
        {
            textoFinal.SetText("Quedaste!" + posicionTM.GetComponent<TextMeshProUGUI>().text);
            returnMenu = 2 + Time.time;
            //Application.LoadLevel("Menu");
        }
    }
}