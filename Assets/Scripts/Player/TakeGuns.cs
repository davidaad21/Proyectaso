﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeGuns : MonoBehaviour
{

    public GameObject Arma1Point;

    private GameObject pickedObjet = null;

    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("ArmaD") && pickedObjet == null)
        {
            other.transform.position = Arma1Point.transform.position;
            other.gameObject.transform.SetParent(Arma1Point.gameObject.transform);

            pickedObjet = other.gameObject;

        }
    }
    public bool vacio()
    {
        return pickedObjet;
    }
}
