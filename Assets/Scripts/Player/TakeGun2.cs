﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeGun2 : MonoBehaviour
{
    public GameObject Arma2Point;

    private GameObject pickedObjet2 = null;
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Arma") && pickedObjet2 == null)
        {
            other.transform.position = Arma2Point.transform.position;
            other.gameObject.transform.SetParent(Arma2Point.gameObject.transform);

            pickedObjet2 = other.gameObject;
        }
    }
    public bool vacio()
    {
        return pickedObjet2;
    }
}
