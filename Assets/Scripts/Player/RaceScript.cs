﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaceScript : MonoBehaviour
{
    public TextMeshProUGUI PosicionG;
    public TextMeshProUGUI mensajeFinal;
    public GameObject auto1;
    public GameObject auto2;
    public GameObject auto3;
    public GameObject auto4;
    private int currentCheckAuto1 = 0;
    private int currentCheckAuto2 = 0;
    private int currentCheckAuto3 = 0;
    private int currentCheckAuto4 = 0;
    private double distanceNxtCheck1 = 0;
    private double distanceNxtCheck2 = 0;
    private double distanceNxtCheck3 = 0;
    private double distanceNxtCheck4 = 0;
    private int posicion_final = 1;
    private ArrayList distancias;

    public GameObject checkpointContainer;
    private Transform[] checkpoints;
    void GetCheckpoints()
    {
        Transform[] potencialcheckpoints = checkpointContainer.GetComponentsInChildren<Transform>();
        checkpoints = new Transform[potencialcheckpoints.Length - 1];
        int i = 0;
        foreach (Transform potencialWaypoint in potencialcheckpoints)
            if (potencialWaypoint != checkpointContainer.transform)
                checkpoints[i++] = potencialWaypoint;
    }
    // Start is called before the first frame update
    void Start()
    {
        GetCheckpoints();
    }

    // Update is called once per frame
    void Update()
    {
        posicionamiento();
        Vector2 p1;
        Vector2 p2;
        p1 = new Vector2(auto1.transform.position.x, auto1.transform.position.z);
        p2 = new Vector2(checkpoints[currentCheckAuto1].position.x, checkpoints[currentCheckAuto1].position.z);
        distanceNxtCheck1 = checkpoints.Length - currentCheckAuto1 - ((Vector2.Distance(p1, p2) - 5) * (-0.001));
        p1 = new Vector2(auto2.transform.position.x, auto2.transform.position.z);
        p2 = new Vector2(checkpoints[currentCheckAuto2].position.x, checkpoints[currentCheckAuto2].position.z);
        distanceNxtCheck2 = checkpoints.Length - currentCheckAuto2 - ((Vector2.Distance(p1, p2) - 5) * (-0.001));
        p1 = new Vector2(auto3.transform.position.x, auto3.transform.position.z);
        p2 = new Vector2(checkpoints[currentCheckAuto3].position.x, checkpoints[currentCheckAuto3].position.z);
        distanceNxtCheck3 = checkpoints.Length - currentCheckAuto3 - ((Vector2.Distance(p1, p2) - 5) * (-0.001));
        p1 = new Vector2(auto4.transform.position.x, auto4.transform.position.z);
        p2 = new Vector2(checkpoints[currentCheckAuto4].position.x, checkpoints[currentCheckAuto4].position.z);
        distanceNxtCheck4 = checkpoints.Length - currentCheckAuto4 - ((Vector2.Distance(p1, p2) - 5) * (-0.001));

        double[] a = new double[4] { distanceNxtCheck1, distanceNxtCheck2, distanceNxtCheck3, distanceNxtCheck4 };
        for (int x = 0; x < a.Length-1; x++)
        {
            for (int k = 0; k < a.Length-1 - x;k++ )
            {
                if (a[k] > a[k + 1])
                {
                    double aux;
                    aux = a[k];
                    a[k] = a[k + 1];
                    a[k + 1] = aux;
                }
            }
        }
        for (int i = 0; i <= 3; i++)
        {
            if (distanceNxtCheck1 == a[i])
            {
                PosicionG.SetText("#" + (i+1));
            }
        }
        
    }

    public void posicionamiento()
    {
        Vector3 RelativeWaypointPosition1 = auto1.transform.InverseTransformPoint(new Vector3(
            checkpoints[currentCheckAuto1].position.x,
            checkpoints[currentCheckAuto1].position.y,
            checkpoints[currentCheckAuto1].position.z));

        if (RelativeWaypointPosition1.magnitude < 5)
        {
            if (currentCheckAuto1 < checkpoints.Length - 1)
            {
                currentCheckAuto1++;
            }
            else
            {
                currentCheckAuto1 = 0;
            }
        }
        Vector3 RelativeWaypointPosition2 = auto2.transform.InverseTransformPoint(new Vector3(
            checkpoints[currentCheckAuto2].position.x,
            checkpoints[currentCheckAuto2].position.y,
            checkpoints[currentCheckAuto2].position.z));

        if (RelativeWaypointPosition2.magnitude < 5)
        {
            if (currentCheckAuto2 < checkpoints.Length - 1)
            {
                currentCheckAuto2++;
            }
            else
            {
                currentCheckAuto2 = 0;
            }
        }
        Vector3 RelativeWaypointPosition3 = auto3.transform.InverseTransformPoint(new Vector3(
            checkpoints[currentCheckAuto3].position.x,
            checkpoints[currentCheckAuto3].position.y,
            checkpoints[currentCheckAuto3].position.z));

        if (RelativeWaypointPosition3.magnitude < 5)
        {
            if (currentCheckAuto3 < checkpoints.Length-1)
            {
                currentCheckAuto3++;
            }
            else
            {
                currentCheckAuto3 = 0;
            }
        }
        Vector3 RelativeWaypointPosition4 = auto4.transform.InverseTransformPoint(new Vector3(
            checkpoints[currentCheckAuto4].position.x,
            checkpoints[currentCheckAuto4].position.y,
            checkpoints[currentCheckAuto4].position.z));

        if (RelativeWaypointPosition4.magnitude < 5)
        {
            if (currentCheckAuto4 < checkpoints.Length - 1)
            {
                currentCheckAuto4++;
            }
            else
            {
                currentCheckAuto4 = 0;
            }
        }
    }
}