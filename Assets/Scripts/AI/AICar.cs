﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

[System.Serializable]
public class AxleInfoAI
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;

    public Transform visualLeftWheel;
    public Transform visualRightWheel;

    public bool motor;
    public bool steering;
}

public class AICar : MonoBehaviour
{
    public List<AxleInfoAI> axleInfos;
    public float forcePrimeraMarcha;
    public float forceSegundaMarcha;
    public float forceUltimaMarcha;
    public float maxVelocityFirst = 30;
    public float maxVelocitySecond = 60;
    public float maxVelocityFinal = 80;
    public float maxSteeringAngle = 45;
    private Rigidbody _rb;
    //Borrar texto de vida del enemigo
    public GameObject objBala;
    public AudioClip sonidoDisparo;
    AudioSource audioS;

    public GameObject waypointContainer;
    private Transform[] waypoints;
    private int currentWaypoint = 0;
    private float steering = 0;
    private float motor = 0;

    public float timeBeforeNextShot = 0.2f;
    private float canShot = 0f;
    public float disparoAleatorio;
    public float salud = 10;

    void Start()
    {
        audioS = GetComponent<AudioSource>();
        disparoAleatorio = Random.Range(5.0f, 8.0f);
        _rb = GetComponent<Rigidbody>();
        GetWaypoints();
    }

    public void ApplyLocalPositionToVisuals(WheelCollider collider, Transform visualWheel)
    {
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate()
    {
        NavigateToWayPoint();
        float velocidad = Mathf.RoundToInt(_rb.velocity.magnitude * 3600 / 1000);
        float motor2;
        if (velocidad < maxVelocityFirst)
        {
            motor2 = forcePrimeraMarcha * motor;
        }
        else
        {
            if (velocidad > maxVelocitySecond)
            {
                motor2 = forceUltimaMarcha * motor;
            }
            else
            {
                motor2 = forceSegundaMarcha * motor;
            }

        }
        float steering2 = maxSteeringAngle * steering;

        if (Time.time > disparoAleatorio && Time.time > canShot)
        {
            audioS.PlayOneShot(sonidoDisparo);
            canShot = Time.time + timeBeforeNextShot;
            GameObject balaP0 = Instantiate(objBala, transform.GetChild(0).position + new Vector3(0, 0, 0), Quaternion.identity);
            GameObject balaP1 = Instantiate(objBala, transform.GetChild(1).position + new Vector3(0, 0, 0), Quaternion.identity);
            Rigidbody balaPrefabInstanc0 = balaP0.GetComponent<Rigidbody>();
            Rigidbody balaPrefabInstanc1 = balaP1.GetComponent<Rigidbody>();
            balaPrefabInstanc0.AddForce(transform.forward * 100 * 30);
            balaPrefabInstanc1.AddForce(transform.forward * 100 * 30);
            Destroy(balaP0, 4.0f);
            Destroy(balaP1, 4.0f);
            disparoAleatorio = Time.time + Random.Range(3.0f, 8.0f);
        }

        foreach (AxleInfoAI axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering2;
                axleInfo.rightWheel.steerAngle = steering2;
            }
            if (axleInfo.motor)
            {
                if (velocidad > maxVelocityFinal)
                 {
                    axleInfo.leftWheel.motorTorque = 0;
                    axleInfo.rightWheel.motorTorque = 0;
                }
                else
                {
                    axleInfo.leftWheel.motorTorque = motor2;
                    axleInfo.rightWheel.motorTorque = motor2;
                }
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel, axleInfo.visualLeftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel, axleInfo.visualRightWheel);
        }
    }
    private void AnimateWheels(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Quaternion _rot;
        Vector3 _pos;

        Vector3 rotate = Vector3.zero;
        wheelCollider.GetWorldPose(out _pos, out _rot);
        wheelTransform.transform.rotation = _rot;
    }

    void GetWaypoints()
    {
        Transform[] potencialwaypoints = waypointContainer.GetComponentsInChildren<Transform>();
        waypoints = new Transform[potencialwaypoints.Length-1];
        int i = 0;
        foreach (Transform potencialWaypoint in potencialwaypoints)
            if (potencialWaypoint != waypointContainer.transform)
                waypoints[i++] = potencialWaypoint;
    }
    void NavigateToWayPoint()
    {
        Vector3 RelativeWaypointPosition = transform.InverseTransformPoint(new Vector3(
            waypoints[currentWaypoint].position.x, 
            transform.position.y,
            waypoints[currentWaypoint].position.z));
        steering = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude;
        if(steering < 0.5f)
        {
            motor = RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - Mathf.Abs(steering);
        }
        else
        {
            motor = 0;
        }
        if (RelativeWaypointPosition.magnitude < 10)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Length)
            {
                currentWaypoint = 0;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.IndexOf("Bala") != -1 || collision.gameObject.name.IndexOf("Bullet") != -1)
        {
            salud --;
            if(salud <= 0)
            {
                forcePrimeraMarcha = 0;
                forceSegundaMarcha = 0;
                forceUltimaMarcha = 0;
}
        }
    }
}