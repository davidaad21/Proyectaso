﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawWPGizmos : MonoBehaviour
{
    void OnDrawGizmos()
    {
        Transform[] waypoints = this.GetComponentsInChildren<Transform>();
        foreach (Transform waypoint in waypoints)
            Gizmos.DrawSphere(waypoint.position, 1.0f);
    }
}